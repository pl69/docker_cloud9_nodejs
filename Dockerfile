FROM debian:wheezy
MAINTAINER pl@xxxx.com

# To get rid of error messages like "debconf: unable to initialize frontend: Dialog":
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# System packages update
RUN apt-get -q update
RUN apt-get -qy dist-upgrade

RUN apt-get autoremove &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    rm -rf /tmp/*


ADD start.sh /start.sh

VOLUME ["/workspace"]


# WebUI
#EXPOSE 8112

